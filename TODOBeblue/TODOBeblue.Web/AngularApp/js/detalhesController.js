﻿(function (app) {

    var detalhesController = function ($scope, $http, $routeParams) {
        var id = $routeParams.id;
        $http.get("/api/filme/" + id)
        .success(function (data) {
            $scope.filme = data;
        });
    };

    app.controller("detalhesController", detalhesController);

}(angular.module("tarefa")));