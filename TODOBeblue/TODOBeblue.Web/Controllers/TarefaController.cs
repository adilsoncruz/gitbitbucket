﻿using System;
using System.Collections.Generic;

using System.Net;

using System.Web.Http;
using System.Web.Http.Description;
using TODOBeblue.App;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.IApp;


namespace TODOBeblue.Web.Controllers
{
    public class TarefaController : ApiController
    {

        private readonly ITarefaApp _tarefaApp;
        public TarefaController(ITarefaApp tarefaApp)
        {
            _tarefaApp = tarefaApp;
        }


        // GET: api/Tarefa
        public IEnumerable<Tarefas> Get()
        {
            var tarefa = _tarefaApp.GetAll();
            return tarefa;
        }

        // GET: api/Tarefa/5
        public IHttpActionResult Get(int id)
        {
            var tarefa = _tarefaApp.GetById(id);
            return Ok(tarefa);
        }

        // POST: api/Tarefa
        [ResponseType(typeof(Tarefas))]
        public IHttpActionResult Post(Tarefas tarefas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _tarefaApp.Salvar(tarefas);

            return CreatedAtRoute("DefaultApi", new { id = tarefas.Id }, tarefas);
        }

        // PUT: api/Tarefa/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, Tarefas tarefas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tarefas.Id)
                return BadRequest();

            try
            {
                _tarefaApp.Salvar(tarefas);
                _tarefaApp.Commit();
            }
            catch (Exception)
            {
                throw;
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Tarefa/5
        [ResponseType(typeof(Tarefas))]
        public IHttpActionResult Delete(int id)
        {
            var tarefa = _tarefaApp.Get(id);

            if (tarefa == null)
            {
                return NotFound();

            }
            _tarefaApp.Delete(id);
            _tarefaApp.Commit();
            return Ok(tarefa);
        }
    }
}
