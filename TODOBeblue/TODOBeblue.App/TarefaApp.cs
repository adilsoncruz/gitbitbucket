﻿

using System.Linq;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.IApp;
using TODOBeblue.Domain.IRepositories;

namespace TODOBeblue.App
{
    public class TarefaApp : ITarefaApp
    {

        private readonly ITarefasRepository _tarefaRepository;

        public TarefaApp(ITarefasRepository tarefaRepository)
        {
            _tarefaRepository = tarefaRepository;
        }

        public Tarefas Get(int id)
        {
            return _tarefaRepository.Get(id);
        }

        public Tarefas Get(string nomeTarefa)
        {
            return _tarefaRepository.Get(nomeTarefa);
        }

        public IQueryable<Tarefas> GetAll()
        {
            return _tarefaRepository.GetAll();
        }

        public void Salvar(Tarefas tarefas)
        {
            _tarefaRepository.Salvar(tarefas);
        }
        
        public IQueryable<Tarefas> GetById(int id)
        {
           return _tarefaRepository.GetById(id);
        }


        public void Delete(Tarefas obj)
        {
            _tarefaRepository.Delete(obj);
        }

        public void Delete(int id)
        {
            _tarefaRepository.Delete(id);
        }


        public void DeleteAll(System.Collections.Generic.IEnumerable<Tarefas> obj)
        {
            _tarefaRepository.DeleteAll(obj);
        }


        public void Commit()
        {
            _tarefaRepository.Commit();
        }
    }
}
