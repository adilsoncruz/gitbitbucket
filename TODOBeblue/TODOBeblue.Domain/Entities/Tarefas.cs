﻿

using System;
using TODOBeblue.Domain.ValueObject;
using TODOBeblue.Helpers;

namespace TODOBeblue.Domain.Entities
{
    public class Tarefas : EntityBase
    {
        public int Id { get; set; }

        public const int NomeTarefaMaxLength = 100;
        public string NomeTarefa { get; private set; }

        public const int DescricaoMaxLength = 250;
        public string Descricao { get; private set; }

        public bool Finalizado { get; private set; }

        public virtual Usuario Usuario { get; private set; }

        public virtual Categoria Categoria { get; private set; }

        public DateTime? DataFinalizacao { get; set; }

        //EntityFramework
        protected Tarefas(){}

        public Tarefas(string nomeTarefa, string descricao, bool finalizado, Categoria categoria, Usuario usuario)
        {
            SetNomeTarefa(nomeTarefa);
            SetDescricao(descricao);
            SetFinalizado(finalizado);
            SetCategoria(categoria);
            SetUsuario(usuario);
            DataInclusao = DateTime.Now;
            

        }


        public void SetNomeTarefa(string nomeTarefa)
        {
            Guard.ForNullOrEmptyDefaultMessage(nomeTarefa, "Nome da tarefa");
            Guard.StringLength("Nome da tarefa", nomeTarefa, NomeTarefaMaxLength);
            NomeTarefa = nomeTarefa;
        }

        public void SetDescricao(string descricao)
        {
            Guard.ForNullOrEmptyDefaultMessage(descricao, "Descrição");
            Guard.StringLength("Descrição", descricao, NomeTarefaMaxLength);
            Descricao = descricao;
        }

        public void SetCategoria(Categoria categoria)
        {
            if(categoria == null)
                throw  new Exception("Categoria é obrigatório");
        }

        public void SetUsuario(Usuario usuario)
        {
            if(usuario == null)
                throw new Exception("Usuário é obrigatório");
        }

        public void SetFinalizado(bool finalizado)
        {
            Finalizado = finalizado;
        }
    }
}
