﻿

using System;

namespace TODOBeblue.Domain.Entities
{
    public class EntityBase
    {
        public int Id { get; set; }

        public DateTime DataInclusao { get; set; }

        public DateTime? DataAtualizacao { get; set; }

       
    }
}
