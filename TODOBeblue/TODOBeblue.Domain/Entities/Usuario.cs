﻿

using System;
using System.Collections.Generic;
using TODOBeblue.Helpers;

namespace TODOBeblue.Domain.Entities
{
    public class Usuario : EntityBase
    {
        public int Id { get; private set; }

        public string NomeUsuario { get; private set; }

        public const int NomeUsuarioMaxLength = 250;

        //EntityFramework
        protected Usuario(){}


        public Usuario(string nomeUsuario)
        {
            SetNomeUsuario(nomeUsuario);
        }

        public void SetNomeUsuario(string nomeUsuario)
        {
            Guard.ForNullOrEmptyDefaultMessage(nomeUsuario, "Nome Usuário");
            Guard.StringLength("Nome do Usuário", nomeUsuario, NomeUsuarioMaxLength);
            NomeUsuario = nomeUsuario;
            DataInclusao = DateTime.Now;
            ;
        }



    }
}
