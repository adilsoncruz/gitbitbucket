﻿

using System;
using System.Collections.Generic;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Helpers;

namespace TODOBeblue.Domain.ValueObject
{
    public class Categoria : EntityBase
    {
        public int Id { get; private set; }

        public string NomeCategoria { get; private set; }

        public const int CategoriaMaxLength = 7;

        //EntityFramework
        protected Categoria(){}

        public Categoria(string nomeCategoria)
        {
            SetNomeCategoria(nomeCategoria);
        }

        /// <summary>
        /// Método responsável por validar o nome da categoria
        /// </summary>
        /// <param name="nomeCtegoria">Nome da categoria</param>
        public void SetNomeCategoria(string nomeCtegoria)
        {
            Guard.ForNullOrEmptyDefaultMessage(nomeCtegoria, "Nome da Categoria");
            Guard.StringLength("Nome da Categoria", nomeCtegoria, CategoriaMaxLength);
            NomeCategoria = nomeCtegoria;
            DataInclusao = DateTime.Now;
        }
    }
}
