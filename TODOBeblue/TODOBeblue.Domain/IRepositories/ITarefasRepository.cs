﻿

using System.Collections.Generic;
using System.Linq;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Domain.IRepositories
{
    public interface ITarefasRepository
    {
        Tarefas Get(int id);
        Tarefas Get(string nomeTarefa);
        IQueryable<Tarefas> GetByUsuario(Usuario usuario);
        IQueryable<Tarefas> GetByUaurio(string nomeUsuario);
        IQueryable<Tarefas> GetByCategoria(Categoria categoria);
        IQueryable<Tarefas> GetByCategoria(string nomeCategoria);
        IQueryable<Tarefas> GetAll();
        IQueryable<Tarefas> GetById(int id);
        void Salvar(Tarefas tarefas);
        void Delete(Tarefas obj);
        void Delete(int id);
        void DeleteAll(IEnumerable<Tarefas> obj);
        void Commit();
    }
}
