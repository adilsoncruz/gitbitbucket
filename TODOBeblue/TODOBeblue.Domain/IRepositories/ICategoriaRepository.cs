﻿

using System.Linq;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Domain.IRepositories
{
    public interface ICategoriaRepository
    {
        Categoria Get(int id);
        Categoria Get(string nome);
        IQueryable<Categoria> Get();
        void Salvar(Categoria categoria);
    }
}
