﻿

using TODOBeblue.Domain.Entities;

namespace TODOBeblue.Domain.IRepositories
{
    public interface IUsuarioRepository
    {
        Usuario Get(string nome);
        Usuario Get(int id);
        void Salvar(Usuario usuario);
    }
}
