﻿

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Domain.Test.ValueObject
{
    [TestClass]
    public class CategoriaTest
    {
        
        [TestMethod]
        [ExpectedException(typeof (Exception))]
        public void Categoria_New_Em_Branco()
        {
            new Categoria("");
        }


        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Categoria_New_Null()
        {
            new Categoria(null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Categoria_Error_MaxLength()
        {
            var nomeCategoria = "À Fazer";

            while (nomeCategoria.Length < Categoria.CategoriaMaxLength + 1)
            {
                nomeCategoria += "À Fazer";
            }

            new Categoria(nomeCategoria);
        }

        [TestMethod]
        public void Categoria_New_Valido()
        {
            var nomeCategoria = "Todos";
            var categoria = new Categoria(nomeCategoria);
            Assert.AreEqual(nomeCategoria, categoria.NomeCategoria);
        }


    }
}
