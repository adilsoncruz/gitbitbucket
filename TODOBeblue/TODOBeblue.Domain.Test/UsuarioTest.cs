﻿

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Domain.Test
{
    [TestClass]
    public class UsuarioTest
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Usuario_New_Em_Branco()
        {
            new Usuario("");
        }


        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Usuario_New_Null()
        {
            new Usuario(null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Usuario_Error_MaxLength()
        {
            var nomeUsuario = "À Fazer";

            while (nomeUsuario.Length < Usuario.NomeUsuarioMaxLength + 1)
            {
                nomeUsuario += "À Fazer";
            }

            new Usuario(nomeUsuario);
        }

        [TestMethod]
        public void Categoria_New_Valido()
        {
            var nomeUsuario = "Todos";
            var usuario = new Usuario(nomeUsuario);
            Assert.AreEqual(nomeUsuario, usuario.NomeUsuario);
        }
    }
}
