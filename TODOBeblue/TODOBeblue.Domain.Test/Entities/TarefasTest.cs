﻿

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Domain.Test.Entities
{
    [TestClass]
    public class TarefasTest
    {
        public string NomeTarefa { get;  set; }

        public string Descricao { get;  set; }

        public bool Finalizado { get;  set; }

        public Usuario Usuario { get; set; }

        public Categoria Categoria { get; set; }


        public TarefasTest()
        {
            NomeTarefa = "Atualizar Cadastro";
            Descricao = "Atualizar o cadastro de compra do cliente Pedro";
            Finalizado = false;

            Usuario = new Usuario("Adilson");

            Categoria = new Categoria("À Fazer");

        }

        #region Nome da tarefa

        [TestMethod]
        [ExpectedException(typeof (Exception))]
        public void Taferas_NomeTarefa_New_Em_Branco()
        {
            new Tarefas("", Descricao, Finalizado, Categoria, Usuario);
        }


        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Tarefas_NomeTarefa_New_Null()
        {
            new Tarefas(null, Descricao, Finalizado, Categoria, Usuario);
            
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Tarefas_NomeTarefa_Error_MaxLength()
        {
            var nomeTarefa = "Pegar o log de erro do IIS";

            while (nomeTarefa.Length < Tarefas.NomeTarefaMaxLength + 1)
            {
                nomeTarefa += "Pegar o log de erro do IIS";
            }

            new Tarefas(nomeTarefa, Descricao, Finalizado, Categoria, Usuario);
            
        }

        [TestMethod]
        public void Tarefa_NomeTarefa_New_Valido()
        {
            var nomeTarefa = "Copiar o arquivo WEB.config para o diretório d:";
            var tarefa = new Tarefas(nomeTarefa, Descricao, Finalizado, Categoria, Usuario);
            Assert.AreEqual(nomeTarefa, tarefa.NomeTarefa);
        }


        #endregion

        #region Descricao

        [TestMethod]
        [ExpectedException(typeof (Exception))]
        public void Taferas_Descricao_New_Em_Branco()
        {
            new Tarefas(NomeTarefa, "", Finalizado, Categoria, Usuario);
            
        }


        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Tarefas_Descricao_New_Null()
        {
            new Tarefas(NomeTarefa, null, Finalizado, Categoria, Usuario);
            
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Tarefas_Descricao_Error_MaxLength()
        {
            var descricao = "Pegar o log de erro do IIS e verificar o erro que ocorreu em produção.";

            while (descricao.Length < Tarefas.DescricaoMaxLength + 1)
            {
                descricao += "Pegar o log de erro do IIS e verificar o erro que ocorreu em produção.";
            }

            new Tarefas(NomeTarefa, descricao, Finalizado, Categoria, Usuario);
            
        }

        [TestMethod]
        public void Tarefa_Descricao_New_Valido()
        {
            var descricao = "Pegar o log de erro do IIS e verificar o erro que ocorreu em produção.";
            var tarefa = new Tarefas(NomeTarefa, descricao, Finalizado, Categoria, Usuario);
            
            Assert.AreEqual(descricao, tarefa.Descricao);
        }



        #endregion







    }
}
