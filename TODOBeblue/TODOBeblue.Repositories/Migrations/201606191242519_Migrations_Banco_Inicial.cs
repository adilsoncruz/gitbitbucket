namespace TODOBeblue.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migrations_Banco_Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categoria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NomeCategoria = c.String(nullable: false, maxLength: 7, unicode: false),
                        DataInclusao = c.DateTime(nullable: false),
                        DataAtualizacao = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.NomeCategoria, unique: true);
            
            CreateTable(
                "dbo.Tarefas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NomeTarefa = c.String(nullable: false, maxLength: 100, unicode: false),
                        Descricao = c.String(nullable: false, maxLength: 250, unicode: false),
                        Finalizado = c.Boolean(nullable: false),
                        DataFinalizacao = c.DateTime(),
                        DataInclusao = c.DateTime(nullable: false),
                        DataAtualizacao = c.DateTime(),
                        IdCategoria = c.Int(nullable: false),
                        IdUsuario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categoria", t => t.IdCategoria)
                .ForeignKey("dbo.Usuario", t => t.IdUsuario)
                .Index(t => t.IdCategoria)
                .Index(t => t.IdUsuario);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NomeUsuario = c.String(maxLength: 250, unicode: false),
                        DataInclusao = c.DateTime(nullable: false),
                        DataAtualizacao = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.NomeUsuario, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tarefas", "IdUsuario", "dbo.Usuario");
            DropForeignKey("dbo.Tarefas", "IdCategoria", "dbo.Categoria");
            DropIndex("dbo.Usuario", new[] { "NomeUsuario" });
            DropIndex("dbo.Tarefas", new[] { "IdUsuario" });
            DropIndex("dbo.Tarefas", new[] { "IdCategoria" });
            DropIndex("dbo.Categoria", new[] { "NomeCategoria" });
            DropTable("dbo.Usuario");
            DropTable("dbo.Tarefas");
            DropTable("dbo.Categoria");
        }
    }
}
