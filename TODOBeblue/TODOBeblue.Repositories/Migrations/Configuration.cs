using TODOBeblue.Repositories.Seeds;

namespace TODOBeblue.Repositories.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TODOBeblue.Repositories.EfDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TODOBeblue.Repositories.EfDbContext context)
        {
            //UsuarioSeed.Seed(context);
            //CategoriaSeed.Seed(context);
            //TarefaSeed.Seed(context);
        }
    }
}
