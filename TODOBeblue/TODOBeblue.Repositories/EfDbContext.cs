﻿

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.ValueObject;
using TODOBeblue.Repositories.EntityTypeConfigurations;

namespace TODOBeblue.Repositories
{
    public class EfDbContext : DbContext
    {
        public EfDbContext()
            : base("TodoBeblue")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Tarefas> Tarefas { get; set; }
        
        


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        
            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("varchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(50));

            modelBuilder.Configurations.Add(new CategoriaConfigurations());
            modelBuilder.Configurations.Add(new UsuarioConfigurations());
            modelBuilder.Configurations.Add(new TarefasConfigurations());

        }
    }
}
