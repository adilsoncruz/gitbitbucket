﻿

using System;
using System.Linq;
using TODOBeblue.Domain.IRepositories;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Repositories
{
    public class CategoriaRepositoty : ICategoriaRepository
    {

        private readonly IRepository<Categoria> _categoriaRepository;

        public CategoriaRepositoty(IRepository<Categoria> categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }

        public Categoria Get(int id)
        {
            return _categoriaRepository.Get(id);
        }

        public Categoria Get(string nome)
        {
            return
                _categoriaRepository.Get()
                    .FirstOrDefault(x => string.Equals(x.NomeCategoria, nome, StringComparison.CurrentCultureIgnoreCase));
        }

        public void Salvar(Categoria categoria)
        {
            _categoriaRepository.AddOrUpdate(categoria);
            _categoriaRepository.Commit();
        }


        public IQueryable<Categoria> Get()
        {
            return _categoriaRepository.Get();
        }
    }
}
