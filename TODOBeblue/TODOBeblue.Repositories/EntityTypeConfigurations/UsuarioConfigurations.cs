﻿

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using TODOBeblue.Domain.Entities;

namespace TODOBeblue.Repositories.EntityTypeConfigurations
{
    public class UsuarioConfigurations : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfigurations()
        {
            Property(x => x.NomeUsuario)
                .HasColumnName("NomeUsuario")
                .HasMaxLength(Usuario.NomeUsuarioMaxLength)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_NomeUsuario", 1){IsUnique = true}));
        }
    }
}
