﻿

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Repositories.EntityTypeConfigurations
{
    public class CategoriaConfigurations : EntityTypeConfiguration<Categoria>
    {
        public CategoriaConfigurations()
        {
            ToTable("Categoria");

            HasKey(x => x.Id);

            Property(x => x.NomeCategoria)
                .HasColumnName("NomeCategoria")
                .IsRequired()
                .HasMaxLength(Categoria.CategoriaMaxLength)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("IX_NomeCategoria",1){IsUnique = true}));
        }
    }
}
