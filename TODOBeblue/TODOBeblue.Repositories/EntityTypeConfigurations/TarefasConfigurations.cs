﻿

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using TODOBeblue.Domain.Entities;

namespace TODOBeblue.Repositories.EntityTypeConfigurations
{
    public class TarefasConfigurations : EntityTypeConfiguration<Tarefas>
    {
        public TarefasConfigurations()
        {

            ToTable("Tarefas");

            HasKey(x => x.Id);

            Property(x => x.NomeTarefa)
                .HasColumnName("NomeTarefa")
                .HasMaxLength(Tarefas.NomeTarefaMaxLength)
                .IsRequired();

            Property(x => x.Descricao)
                .HasColumnName("Descricao")
                .HasMaxLength(Tarefas.DescricaoMaxLength)
                .IsRequired();

            Property(x => x.Finalizado)
                .HasColumnName("Finalizado")
                .IsRequired();


            HasRequired(x => x.Usuario)
                .WithMany() //Usuario pode ter muitas tarefas
                .Map(m => m.MapKey("IdUsuario"));//a chave estrangeira em Tarefas é IdUsuario

            HasRequired(c => c.Categoria)
                .WithMany()
                .Map(m => m.MapKey("IdCategoria"));

        }
    }
}
