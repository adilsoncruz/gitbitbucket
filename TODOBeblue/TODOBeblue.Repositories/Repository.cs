﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.IRepositories;

namespace TODOBeblue.Repositories
{
    public class Repository<Tentity> : IRepository<Tentity> where Tentity : EntityBase
    {
        protected readonly EfDbContext Context;

        public Repository(EfDbContext context)
        {
            Context = context;
        }

        private DbSet<Tentity> Entity { get { return Context.Set<Tentity>(); }}


        public void Add(Tentity obj)
        {
            obj.DataInclusao = DateTime.Now;
            Entity.Add(obj);
        }

        public void AddAll(IEnumerable<Tentity> obj)
        {
            foreach (var entity in obj)
            Add(entity);
        }

        public void DeleteAll(IEnumerable<Tentity> obj)
        {
            foreach (var entity in obj)
            Delete(entity);
        }

        public void Delete(Tentity obj)
        {
            Entity.Remove(obj);
        }

        public void Delete(int id)
        {
            Entity.Remove(Get(id));
        }

        public Tentity Get(int id)
        {
            return Entity.Find(id);
        }

        public Tentity First()
        {
            return Entity.FirstOrDefault();
        }

        public IQueryable<Tentity> Get()
        {
            return Entity;
        }

        public void Update(Tentity obj)
        {
            obj.DataAtualizacao = DateTime.Now;
            Context.Entry(obj).State = EntityState.Modified;
        }

        public void AddOrUpdate(Tentity obj)
        {
            if (obj.Id > 0)
                Update(obj);
            else
                Add(obj);
        }

        public void Commit()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.Write("Entity of type \"{0}\" in state \"{1}\" has the following validtion errors:"
                    , eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\",Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                    
                }
                throw;
            }
        }
    }
}
