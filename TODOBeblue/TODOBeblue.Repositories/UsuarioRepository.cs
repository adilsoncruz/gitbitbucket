﻿
using System;
using System.Linq;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.IRepositories;

namespace TODOBeblue.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {

        private readonly IRepository<Usuario> _usuarioRepository; 

        public UsuarioRepository(IRepository<Usuario> usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }


        public Usuario Get(string nome)
        {
            return _usuarioRepository.Get()
                .FirstOrDefault(x => string.Equals(x.NomeUsuario, nome, StringComparison.CurrentCultureIgnoreCase));
        }

        public Usuario Get(int id)
        {
            return _usuarioRepository.Get(id);
        }

        public void Salvar(Usuario usuario)
        {
            _usuarioRepository.AddOrUpdate(usuario);
            _usuarioRepository.Commit();
        }
    }
}
