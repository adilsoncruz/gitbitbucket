﻿

using System.Data.Entity.Migrations;
using TODOBeblue.Domain.Entities;

namespace TODOBeblue.Repositories.Seeds
{
    public class UsuarioSeed
    {
        public static void Seed(EfDbContext context)
        {
            context.Usuarios.AddOrUpdate(x =>x.Id,new Usuario("Adilson Cruz"));
        }
    }
}
