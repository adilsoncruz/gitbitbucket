﻿

using System.Data.Entity.Migrations;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Repositories.Seeds
{
    public class CategoriaSeed
    {
        public static void Seed(EfDbContext context)
        {
            context.Categorias.AddOrUpdate(x =>x.Id,new Categoria("Todos"));
            context.Categorias.AddOrUpdate(x =>x.Id,new Categoria("Feito"));
            context.Categorias.AddOrUpdate(x =>x.Id,new Categoria("À Fazer"));
        }
    }
}
