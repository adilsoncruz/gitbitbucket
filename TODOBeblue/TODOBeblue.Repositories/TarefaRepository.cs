﻿

using System;
using System.Data.Entity;
using System.Linq;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.IRepositories;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Repositories
{
    public class TarefaRepository : ITarefasRepository
    {

        private readonly IRepository<Tarefas> _tarefaRepository;

        public TarefaRepository(IRepository<Tarefas> tarefaRepository)
        {
            _tarefaRepository = tarefaRepository;
        }


        public Tarefas Get(int id)
        {
           return _tarefaRepository.Get(id);
        }

        public Tarefas Get(string nomeTarefa)
        {
            return
                _tarefaRepository.Get().Include(c => c.Categoria).Include(u => u.Usuario)
                    .FirstOrDefault(
                        x => string.Equals(x.NomeTarefa, nomeTarefa, StringComparison.CurrentCultureIgnoreCase));
        }

        public IQueryable<Tarefas> GetByUsuario(Usuario usuario)
        {
            return _tarefaRepository.Get().Include(x => x.Usuario.Id == usuario.Id);
        }

        public IQueryable<Tarefas> GetByUaurio(string nomeUsuario)
        {
            return _tarefaRepository.Get().Include(x => x.Usuario.NomeUsuario == nomeUsuario);
        }

        public void Salvar(Tarefas tarefas)
        {
            _tarefaRepository.AddOrUpdate(tarefas);
            _tarefaRepository.Commit();
        }
        
        public IQueryable<Tarefas> GetByCategoria(Categoria categoria)
        {
           return _tarefaRepository.Get().Include(x => x.Categoria.Id == categoria.Id);
        }

        public IQueryable<Tarefas> GetByCategoria(string nomeCategoria)
        {
            return _tarefaRepository.Get().Include(x => x.Categoria.NomeCategoria == nomeCategoria);
        }


        public IQueryable<Tarefas> GetAll()
        {
            return _tarefaRepository.Get().Include(c => c.Categoria).Include(u => u.Usuario);
        }


        public IQueryable<Tarefas> GetById(int id)
        {
            return _tarefaRepository.Get().Include(c => c.Categoria).Include(u => u.Usuario).Where(c => c.Id == id);
        }


        public void Delete(Tarefas obj)
        {
            _tarefaRepository.Delete(obj);
        }

        public void Delete(int id)
        {
            _tarefaRepository.Delete(id);
        }


        public void DeleteAll(System.Collections.Generic.IEnumerable<Tarefas> obj)
        {
            _tarefaRepository.DeleteAll(obj);
        }
        
        public void Commit()
        {
            _tarefaRepository.Commit();
        }
    }
}
