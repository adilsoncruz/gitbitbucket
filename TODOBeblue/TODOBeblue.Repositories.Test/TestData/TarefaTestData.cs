﻿

using System.Collections.Generic;
using TODOBeblue.Domain.Entities;
using TODOBeblue.Domain.ValueObject;

namespace TODOBeblue.Repositories.Test.TestData
{
    public class TarefaTestData
    {
        public static Categoria GetCategoria()
        {
            return new Categoria("Todas");
        }

        public static Usuario GetUsuario()
        {
            return new Usuario("Adilson");
        }

        public static List<Tarefas> Get()
        {
            return new List<Tarefas>
            {
                new Tarefas("Olhar Log","Olhar todos os dias o log do usuário",false, GetCategoria(), GetUsuario()),
                new Tarefas("Criar uma nova Tarefa","Criar uma nova tarefa para o usuário Adilson",false, GetCategoria(), GetUsuario()),
                new Tarefas("Subir a nova versão TOTO","Publicar a nova versão do TODO em ambiente de produção",false, GetCategoria(), GetUsuario()),
                new Tarefas("Empacotar os arquivos","Empacotar os arquivos do projeto xpto em enviar ao cliente xyz",false, GetCategoria(), GetUsuario()),
            };
        }


    }
}
